<?php
    function post_product_search(WP_REST_Request $request){
        $productId = $request['productId'];
        $base_url = 'https://B/api/v1/query/' .  $productId;
        // WP ajax to call third party API
        $response = wp_remote_get( $url );
        
        if ( is_array( $response ) && ! is_wp_error( $response ) ) {
            $body    = $response['body'];
            wp_send_json($body);
        }
    }
  
    add_action('rest_api_init', function(){
        register_rest_route( 'v1/', '/product-search', array(
            'methods' => 'POST',
            'callback' => 'post_product_search',
        ));
    });